"""
Написать функцию, строящую дерево по списку пар id (id родителя, id потомка),
где None - id корневого узла.
Пример работы:
"""

source = [
    (None, 'a'),
    (None, 'b'),
    (None, 'c'),
    ('a', 'a1'),
    ('a', 'a2'),
    ('a2', 'a21'),
    ('a2', 'a22'),
    ('b', 'b1'),
    ('b1', 'b11'),
    ('b11', 'b111'),
    ('b', 'b2'),
    ('c', 'c1'),
]

expected = {
    'a': {'a1': {}, 'a2': {'a21': {}, 'a22': {}}},
    'b': {'b1': {'b11': {'b111': {}}}, 'b2': {}},
    'c': {'c1': {}},
}


def get_children(edge_list, parent):
    children = (edge[1] for edge in source if edge[0] == parent)
    return {child: get_children(source, child) for child in children}


def to_tree(edge_list):
    roots = (edge[1] for edge in source if edge[0] is None)
    return {root: get_children(edge_list, root) for root in roots}


assert to_tree(source) == expected
